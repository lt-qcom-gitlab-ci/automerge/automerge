.linux-builders-deps: &linux-builders-deps |
  set -ex

  sudo apt-get update
  sudo apt-get install -y ccache bc kmod cpio chrpath gawk texinfo libsdl1.2-dev whiptail diffstat libssl-dev build-essential libgmp-dev libmpc-dev python3 python3-pip python3-setuptools yamllint libyaml-dev swig rsync python3-dev python3-setuptools-scm python3-wheel

  pip3 install --user dtschema

  # install jflog client tool, v1, used for publishing artifacts
  (mkdir -p $HOME/bin && cd $HOME/bin && curl -fL https://getcli.jfrog.io | sh)

.linux-automerge-builders: &linux-automerge-builders |
  echo "Starting ${JOB_NAME} with the following parameters:"
  echo "AUTOMERGE_REPO_URL: ${AUTOMERGE_REPO_URL}"
  echo "AUTOMERGE_BRANCH: ${AUTOMERGE_BRANCH}"
  echo "CONFIG_REPO_URL: ${CONFIG_REPO_URL}"
  echo "CONFIG_BRANCH: ${CONFIG_BRANCH}"
  echo "KERNEL_REPO_URL: ${KERNEL_REPO_URL}"
  echo "INTEGRATION_REPO_URL: ${INTEGRATION_REPO_URL}"
  echo "INTEGRATION_BRANCH: ${INTEGRATION_BRANCH}"
  echo "KERNEL_CI_REPO_URL: ${KERNEL_CI_REPO_URL}"
  echo "KERNEL_CI_BRANCH: ${KERNEL_CI_BRANCH}"

  git config --global user.name "Linaro Gitlab CI"
  git config --global user.email "ci_notify@linaro.org"

  mkdir -p ${PERSISTENT_PATH}
  KERNEL_REPO_BARE_PATH=${PERSISTENT_PATH}/$(basename ${KERNEL_REPO_URL})
  if [ -d "${KERNEL_REPO_BARE_PATH}" ]; then
        echo "Updating kernel bare repo ..."
        pushd $(pwd)
        cd ${KERNEL_REPO_BARE_PATH}
        if [ -f "gc.log" ]; then
                cat gc.log
                git gc
                git prune
                rm -f gc.log
        fi
        git fetch --all -v
        git update-server-info
        popd
  else
        echo "Cloning integration bare repo ..."
        git clone --bare ${KERNEL_REPO_URL} ${KERNEL_REPO_BARE_PATH}
  fi

  INTEGRATION_REPO_PATH=$(pwd)/$(basename ${INTEGRATION_REPO_URL})
  echo "Cloning integration repo ..."
  git clone ${KERNEL_REPO_BARE_PATH} ${INTEGRATION_REPO_PATH}

  pushd $(pwd)
  export INTG_REMOTE=automerge-intg
  cd ${INTEGRATION_REPO_PATH}

  git remote add ${INTG_REMOTE} ${INTEGRATION_REPO_URL}
  git fetch ${INTG_REMOTE}
  set +e
  git branch -a | grep "remotes/${INTG_REMOTE}/${INTEGRATION_BRANCH}$"
  branch_exists=$?
  if [ $branch_exists -ne 0 ]; then
        echo "Creating initial integration branch ..."
        git push ${INTG_REMOTE} HEAD:${INTEGRATION_BRANCH}
        git fetch -v ${INTG_REMOTE}
  fi
  git checkout -b ${INTEGRATION_BRANCH} ${INTG_REMOTE}/${INTEGRATION_BRANCH}
  popd

  echo "Initializing automerge execution ..."
  pushd $(pwd)
  AUTOMERGE_PATH=$(pwd)/automerge
  git clone ${AUTOMERGE_REPO_URL} -b ${AUTOMERGE_BRANCH} ${AUTOMERGE_PATH}
  export PATH=${AUTOMERGE_PATH}:$PATH

  cd ${AUTOMERGE_PATH}
  export CONFIG_PATH=''
  if [ ! -z ${CONFIG_REPO_URL} ]; then
        export CONFIG_REPO_PATH=${AUTOMERGE_PATH}/$(basename ${CONFIG_REPO_URL})
        git clone ${CONFIG_REPO_URL} -b ${CONFIG_BRANCH} ${CONFIG_REPO_PATH}

        if [ -f ${CONFIG_REPO_PATH}/automerge-ci.conf ]; then
                export CONFIG_PATH=${CONFIG_REPO_PATH}/automerge-ci.conf
        fi
  fi

  if [ -f ${CONFIG_PATH} ]; then
        echo "Using configuration from repository"
        cat ${CONFIG_PATH}
  else
        echo "Using default configuration"
        cat <<EOF > automerge-ci.conf
  baseline git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git master
  EOF

        export CONFIG_PATH=${AUTOMERGE_PATH}/automerge-ci.conf
  fi

  AUTOMERGE_CONFIG=$(sed '/^#/d;/^$/d' ${CONFIG_PATH} | sed ':a;N;$!ba;s/\n/\\n\\\n/g')

  # * Disable exit when fail to collect automerge_result_variables for builders-kernel.sh and email
  # * TODO: Add support in ci-merge to create a log (instead of use tee)
  set +e
  set -o pipefail
  ci-merge -l ${INTEGRATION_REPO_PATH} -r ${INTEGRATION_REPO_URL} -i ${INTEGRATION_BRANCH} -c ${RERERE_REPO_URL} -t ${AUTOMERGE_TRACK:-tag} | tee $CI_PROJECT_DIR/automerge.log
  AUTOMERGE_EXIT_CODE=$?
  set +o pipefail
  AUTOMERGE_BRANCH_FAILED=$(grep 'Merge failed' $CI_PROJECT_DIR/automerge.log | sed ':a;N;$!ba;s/\n/\\n\\\n/g')
  set -e

  cd ${INTEGRATION_REPO_PATH}

  # Extract baseline/master from the config rather than hardcoding it here
  BASELINE=`sed -n -e '/^baseline/{s@[ \t].*[ \t]@/@gp}' ${CONFIG_PATH}`

  # Report some git stats
  PATCH_COUNT=$(git log --oneline --no-merges ${BASELINE}.. |wc -l)
  git shortlog -n --no-merges ${BASELINE}.. > $CI_PROJECT_DIR/wall_of_fame.log

  popd

  echo "AUTOMERGE_CONFIG=${AUTOMERGE_CONFIG}" > $CI_PROJECT_DIR/automerge_result_variables
  echo "AUTOMERGE_BRANCH_FAILED=${AUTOMERGE_BRANCH_FAILED}" >> $CI_PROJECT_DIR/automerge_result_variables
  echo "AUTOMERGE_EXIT_CODE=${AUTOMERGE_EXIT_CODE}" >> $CI_PROJECT_DIR/automerge_result_variables
  echo "INTEGRATION_REPO_PATH=${INTEGRATION_REPO_PATH}" >> $CI_PROJECT_DIR/automerge_result_variables
  echo "PATCH_COUNT=${PATCH_COUNT}" >> $CI_PROJECT_DIR/automerge_result_variables

.linux-automerge-builders-kernel: &linux-automerge-builders-kernel |
  function build_integration_kernel()
  {
        export ARCH=$1
        export KERNEL_CONFIGS=$2

        source install-gcc-toolchain.sh
        export CROSS_COMPILE="ccache $(basename $(ls -1 ${tcbindir}/*-gcc) gcc)"
        export PATH=${tcbindir}:${HOME}/.local/bin:$PATH

        make distclean
        make ${KERNEL_CONFIGS}

        # build QCOM DTBS with warnings
        if [ "$ARCH" = "arm64" ]; then
                make W=1 $(for i in arch/arm64/boot/dts/qcom/*.dts ; do echo qcom/$(basename ${i%.dts}.dtb); done)  2>&1 | tee -a qcom-dtbs.log
        elif [ "$ARCH" = "arm" ]; then
                make W=1 $(for i in arch/arm/boot/dts/qcom*.dts ; do echo $(basename ${i%.dts}.dtb); done)  2>&1 | tee -a qcom-dtbs.log
        fi
        make -j$(nproc)
  }

  if [ ${AUTOMERGE_EXIT_CODE} -ne 0 ]; then
        echo "ERROR: Automerge failed, returned ${AUTOMERGE_EXIT_CODE}"
        if [ ! -z ${KERNEL_CI_REPO_URL} ]; then
                echo "Pushing Automerge working progress branch to ${INTEGRATION_BRANCH_WIP}"
                cd ${INTEGRATION_REPO_PATH}
                git push -f ${KERNEL_CI_REPO_URL} ${INTEGRATION_BRANCH}:${INTEGRATION_BRANCH_WIP}
        fi
        exit ${AUTOMERGE_EXIT_CODE}
  fi

  if [ ! -z "${AUTOMERGE_BRANCH_FAILED}" ]; then
        echo "ERROR: Automerge failed,"
        echo "${AUTOMERGE_BRANCH_FAILED}"
        exit 1
  fi

  pushd ${INTEGRATION_REPO_PATH}

  GIT_STATUS=$(git status -s)
  if [ ! -z "${GIT_STATUS}" ]; then
        echo "ERROR: Automerge repository isn't clean,"
        echo "${GIT_STATUS}"
        exit 1
  fi

  wget https://git.linaro.org/ci/job/configs.git/plain/lt-qcom/install-gcc-toolchain.sh
  build_integration_kernel "arm" "multi_v7_defconfig"
  build_integration_kernel "arm64" "defconfig"

  # record QCOM DTBS warnings, for all builds
  sed -n "s/.*: Warning (\(.*\)):.*/\1/p" qcom-dtbs.log | sort | uniq -c | sort -nr > $CI_PROJECT_DIR/dtbs_warnings.log

  if [ ! -z ${KERNEL_CI_REPO_URL} ]; then
        git push -f ${KERNEL_CI_REPO_URL} ${INTEGRATION_BRANCH}:${KERNEL_CI_BRANCH}
  fi

  popd
  
.automerge:
  stage: automerge
  script:
    - *linux-builders-deps
    - *linux-automerge-builders
    - *linux-automerge-builders-kernel
